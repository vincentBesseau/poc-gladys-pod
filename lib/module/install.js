var fs = require('fs-extra');
var Promise = require('bluebird');
var childProcess = require('child_process');
const Store = require('data-store');
const folder = './config';
const file = 'config.json'
const store = new Store({ path: folder + '/' + file });

module.exports = function install(params){
    if(!params.slug || params.slug.length === 0 || !params.url || params.url.length === 0){
        return Promise.reject(new Error('Slug or url not provided'));
    }
        
    var path = './api/hooks/' + params.slug;
    if( store.data[params.slug] === undefined || store.data[params.slug].state === 'uninstall' ) {
        store.set(params.slug+'.state', 'Cloning')
        console.log(`Cloning module ${params.slug}...`);
        return gitClone(params.url, path)
        .then(function(){
            console.log(`Installing NPM dependencies for module ${params.slug}`);
            return npmInstall(path);
        })
        .then(function(module){
            store.set(params.slug+'.state', 'installed')
            console.log(`Module ${params.slug} installed with success.`);
            return Promise.resolve(params);
        })
        .catch((err) => {
            console.error(`Module installation failed! Cleaning folder`);
            return fs.remove(path).then(() => Promise.reject(err));
        });
    } else {
        return Promise.reject(new Error(`Module ${params.slug} already installed`))
    }
}

/**
 * Clone a repository into a specific folder
 */
function gitClone(url, path){
    return exec(`git clone --depth=1 ${url} ${path}`);
  }
  
  /**
   * NPM install a specific directory
   */
  function npmInstall(path){
    return exec(`npm install --prefix ${path} > /dev/null`);
  }

/**
 * Exec a shell command
 */
function exec(command){
    return new Promise(function(resolve, reject){
      childProcess.exec(command, function (err, stdout, stderr){
        if(err) {
          return reject(err); 
        }
              
        return resolve(stdout);
      });
    });
  }