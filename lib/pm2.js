module.exports = {
	start: require('./pm2/pm2.start'),
	stop: require('./pm2/pm2.stop'),
	restart: require('./pm2/pm2.restart'),
	delete: require('./pm2/pm2.delete'),
    list: require('./pm2/pm2.list'),
    save: require('./pm2/pm2.save')
};
