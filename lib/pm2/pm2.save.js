var Promise = require('bluebird');
var childProcess = require('child_process');


module.exports = function save() {
  return new Promise(function(resolve, reject){
    childProcess.exec('pm2 save', function (err, stdout, stderr){
      if(err) {
        return reject(err); 
      }
            
      return resolve(stdout);
    });
  });
}
