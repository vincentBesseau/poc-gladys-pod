var pm2 = require('pm2');

module.exports = function start(slug) {
  return new Promise((resolve, reject) => {
    pm2.connect(function(err) {
      if (err) {
        process.exit(2);
        reject(err)
      }

      pm2.start({
        script: './api/hooks/' + slug + '/app.js', // Script to be run
        name: slug, // Name under pm2 
      }, function(err, apps) {
        pm2.disconnect(); // Disconnects from PM2
        if (err) reject(err);
        resolve(apps)
      });
    });
  })
}