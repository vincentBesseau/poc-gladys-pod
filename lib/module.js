module.exports = {
	install: require('./module/install'),
	uninstall: require('./module/uninstall'),
	update: require('./module/update'),
	setConfig: require('./module/setConfig'),
};
