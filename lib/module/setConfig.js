const Store = require('data-store');
var fs = require('fs-extra');

const folder = './config';
const file = 'config.json'
const store = new Store({ path: folder + '/' + file });

module.exports = function setConfig(options){

    fs.mkdirs(folder, function(err){
        if (err) return console.error(err);

        options.forEach(element => {
            store.set(element.variable, element.value)
        });

    });
    
}