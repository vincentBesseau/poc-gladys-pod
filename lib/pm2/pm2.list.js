const pm2 = require('pm2')

module.exports = function list() {

  var processList = [];
  return new Promise((resolve, reject) => {

    pm2.connect(function(err) {
      if (err) {
        process.exit(2);
        reject(err)
      }

      pm2.list((err, processDescriptionList) => {
        pm2.disconnect()
        if (err) reject(err)

        for (var idx in processDescriptionList) {
          var name = processDescriptionList[idx]['name'];
          var status = processDescriptionList[idx]['pm2_env']['status']
          processList.push({ 'name': name, 'status': status })
        }
        resolve(processList);
      })
    })
  })
}