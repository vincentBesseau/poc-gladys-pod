var fs = require('fs-extra');
const Promise = require('bluebird');
const Store = require('data-store');
const folder = './config';
const file = 'config.json'
const store = new Store({ path: folder + '/' + file });


module.exports = function uninstall(params){
    if(!params.slug || params.slug.length === 0 || !params.url || params.url.length === 0){
        return Promise.reject(new Error('Slug or url not provided'));
    }
    // getting module state in DB
    if( store.data[params.slug].state === 'installed' || store.data[params.slug].state === 'Cloning' ){
        // we delete the folder
        var modulePath = './api/hooks/' + params.slug;
        console.log(`Module ${params.slug} uninstalled with success.`)
        fs.remove(modulePath);
        
        // changing module state in DB
        store.set(params.slug+'.state', 'uninstall')
        return Promise.resolve(params.slug)
    }
    
}