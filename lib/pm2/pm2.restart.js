const pm2 = require('pm2')

module.exports = function restart(slug) {
  return new Promise((resolve, reject) => {
    pm2.connect(function(err) {
      if (err) {
        process.exit(2);
        reject(err)
      }

      pm2.restart(slug, (err, apps) => {
        pm2.disconnect()
        if (err) reject(err);
        resolve(apps)
      })
    })
  })
}