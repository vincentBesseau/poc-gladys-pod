var fs = require('fs-extra');
const Promise = require('bluebird');
var moduleInstall = require('./install')
const Store = require('data-store');
const folder = './config';
const file = 'config.json'
const store = new Store({ path: folder + '/' + file });

module.exports = function update(params){

    if(!params.slug || params.slug.length === 0 || !params.url || params.url.length === 0){
        return Promise.reject(new Error('Slug or url not provided'));
    }
    // we clean the folder
    var path = './api/hooks/' + params.slug;
    
    return removeDir(path)
    .then(()=>{
        return store.set(params.slug+'.state', 'uninstall')
        .then(()=>{
            console.log(`Update ${params.slug} on gladys POD.`)
            return moduleInstall(params);
        })
    })
    

}

function removeDir(path){
    return new Promise(function(resolve, reject){
        fs.remove(path, function (err) {
            if (err) {
            return reject(err); 
            }
    
            resolve();
        });
    });
  }